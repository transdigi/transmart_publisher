package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
	"transmart_publisher/util"

	"cloud.google.com/go/pubsub"
	"github.com/fsnotify/fsnotify"
	"github.com/joho/godotenv"
	"google.golang.org/api/option"
)

// "time"

// "golang.org/x/net/proxy"

const (
	ProjectId = "dataintegration-072018"
	// watchFolder  = "/home/ctcreport/datainteg/files/training_folder"
	// backupFolder = "/home/ctcreport/datainteg/files/backup_training_folder"
	watchFolder  = "/home/kevin/training_folder"
	backupFolder = "/home/kevin/backup_training_folder"
)

// main
func main() {

	var stackLogs util.StackdriverOutput
	stackLogs.ServiceName = "transmart-publisher-log"
	stackLogs.TimeStamp = time.Now()
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	err := godotenv.Load()
	//watchFolder = os.Getenv("sourcefoldername")
	//destinationFolder = os.Getenv("destinationfoldercopied")
	log.Print(watchFolder)
	// os.Exit(2)
	// for pubsub
	ctx := context.Background()
	proj := ProjectId
	if proj == "" {
		fmt.Fprintf(os.Stderr, "GOOGLE_CLOUD_PROJECT environment variable must be set.\n")
		os.Exit(1)
	}
	client, err := pubsub.NewClient(ctx, ProjectId, option.WithCredentialsFile(util.CredentialPath))
	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}
	const topic = "transmart-topic"
	util.ReadDirectoryFirst(watchFolder, backupFolder, topic, client)

	// if err := publish(client, topic, "hello genks!"); err != nil {
	// 	log.Fatalf("Failed to publish: %v", err)
	// }

	// watch
	// creates a new file watcher
	log.Print("watch folder", watchFolder)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Println("ERROR", err)
	}
	defer watcher.Close()

	//
	done := make(chan bool)

	//
	go func() {
		for {
			select {
			// watch for events
			case event := <-watcher.Events:
				content := util.ProcessingFile(event.Name)
				if content != nil {
					// fmt.Println(client, topic, string(content))
					if err := util.Publish(client, topic, string(content)); err != nil {
						stackLogs.Status = "OK"
						log.Print("Failed to publish: %v", err)
						fileNameSplit := strings.Split(event.Name, "/")
						fileName := fileNameSplit[len(fileNameSplit)-1]
						dest := fmt.Sprintf("%v/%v", backupFolder, fileName)
						os.Rename(event.Name, dest)
						stackLogs.Status = "Error"
						stackLogs.Message = err.Error()
						byteLogs, _ := json.Marshal(stackLogs)
						util.LogToStackDriver(ProjectId, string(byteLogs))
					}
					// log.Print(event.Name)
					os.Remove(event.Name)
					byteLogs, _ := json.Marshal(stackLogs)
					util.LogToStackDriver(ProjectId, string(byteLogs))
				}

				// watch for errors
			case err := <-watcher.Errors:
				log.Println("ERROR", err)
			}
		}
	}()

	// out of the box fsnotify can watch a single file, or a single directory
	if err := watcher.Add(watchFolder); err != nil {
		log.Println("ERROR", err)
	}

	<-done

}
